import 'react-native-gesture-handler';
import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Flickr } from '../screens/home-screen';
import { ResultImages } from '../screens';

const Stack = createStackNavigator();

export const ScreenNavigator = () => {
    return(
    <Stack.Navigator>
        <Stack.Screen name = 'homeScreen' component = {Flickr}   options={{ title: 'Flickr app demo' }}/>
        <Stack.Screen name = 'resultImages' component = {ResultImages}   options={{ title: 'Results'}} /> 
    </Stack.Navigator>
    )
}