import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ScreenNavigator } from './screens-navigator';

const Stack = createStackNavigator();

const RootStack = () =>{
    return(
    <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name='screenStack' component={ScreenNavigator} />
    </Stack.Navigator>
    )
}

export const RootNavigator = () => {
    return(
        <NavigationContainer>
            <RootStack />
        </NavigationContainer>
    )
}