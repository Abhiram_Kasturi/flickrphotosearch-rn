import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

export const Loading = () =>{
    return(
        <View style={styles.screen}>
            <Text>Loading...</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    screen:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})