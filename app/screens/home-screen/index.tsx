import React,{useState} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Button,
} from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { ParamListBase } from '@react-navigation/native';
import { observer } from 'mobx-react';
import {useStores} from '../../models/root-store/root-store-context';

interface HomeScreenProps {
  navigation: StackNavigationProp<ParamListBase>;
}

export const Flickr :React.FC<HomeScreenProps>= observer(({navigation}) =>{
  const {photoStore} = useStores();
  const apicall = () =>{
    photoStore.save(text);
    navigation.navigate('resultImages');
  }
  const restore = () =>{
    changeText("")
    photoStore.reset();
  }
  const [text,changeText] = useState("");
  return (
    <View style={styles.container}>
      <TextInput 
        autoCapitalize={"none"} 
        onChangeText={(value)=>changeText(value)} 
        value={text} 
        style={styles.tinput}
      />
      <View style={styles.button}>
      <Button title="submit" onPress={()=>apicall()}/>
      <Button title="Reset" onPress={()=>restore()}></Button>
      </View>
    </View>
  )
})

const styles = StyleSheet.create({
  tinput : { 
    height: 40, 
    borderColor: 'gray', 
    borderWidth: 1 
  },
  container : {
     marginTop:250
  },
  content : {
    justifyContent:'center',
    alignItems:'center'
  },
  button : {
    flexDirection: 'row', 
    justifyContent: 'center'
  }
})

