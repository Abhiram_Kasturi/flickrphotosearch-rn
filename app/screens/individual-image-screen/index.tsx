import React from 'react';
import {observer} from 'mobx-react';
import {FlatList, Image,StyleSheet,View,Text,ScrollView} from 'react-native';
import {useStores} from '../../models/root-store/root-store-context';

export const ResultImages : React.FC<any> = observer(() =>{
    const {photoStore:{photos}} = useStores();
    console.log(photos);
    return(
        <View style={styles.content}>
            <FlatList
                data={photos}
                keyExtractor={(item)=>item.id}
                renderItem = {({item})=> {
                    return(
                    <Image 
                    style={styles.img}
                    source={{
                    uri: `https://live.staticflickr.com/${item.server_id}/${item.id}_${item.secret}.jpg`
                    }}
                />)} }              
            />
        </View>
    )
})

const styles = StyleSheet.create({
    img:{
        height: 300,
        width:300,
        marginVertical:10
    },
    content : {
    justifyContent:'center',
    alignItems:'center'
  }
})