import React,{useEffect,useState} from 'react';
import {setupRootStore} from './models/root-store/setup-root-store'
import { RootStoreProvider } from './models/root-store/root-store-context';
import { RootNavigator } from './navigation/root-navigator';
import { Loading } from './screens';


const App = () =>{
  const [photoStore,setPhotoStore] = useState<undefined>(undefined);
  useEffect(() =>{
    let photostore = setupRootStore()
    setPhotoStore(photostore);
  },[])

  if(!photoStore){
  return <Loading />
  }

  return(
    <RootStoreProvider value={photoStore}>
      <RootNavigator />
    </RootStoreProvider>
  )
}

export default App;