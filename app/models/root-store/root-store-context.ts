import {createContext, useContext} from 'react';

const RootStoreContext = createContext<any>({} as any);

export const RootStoreProvider = RootStoreContext.Provider;

export const useStores = () => useContext(RootStoreContext);