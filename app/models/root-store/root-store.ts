import {Instance, types} from 'mobx-state-tree';
import {Photos} from '../index';

export const RootStoreModel = types.model('RootStore').props({
    photoStore: types.optional(Photos,{})
});

export interface RootStore extends Instance<typeof RootStoreModel>{}